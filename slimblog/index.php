<?php

session_start();

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('cache/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('cache/error.log', Logger::ERROR));

DB::$host = '127.0.0.1';
DB::$user = 'root';
DB::$password = '';
//DB::$user = 'slimblog';
//DB::$password = 'Jbq2HH9Bn2GzGwzX';
DB::$dbName = 'slimblog';
DB::$error_handler = null;
DB::$nonsql_error_handler = null;
DB::$throw_exception_on_error = true;
DB::$throw_exception_on_nonsql_error = true;

//DB::$error_handler = 'sql_error_handler';
//DB::$nonsql_error_handler = 'nonsql_error_handler';

function sql_error_handler($params) {
    global $app, $log;
    $log->err('SQL error: ' . $params['error']);
    $log->err('SQL query: ' . $params['query']);
    $app->render('error.html.twig');
    die; // don't want to keep going if a query broke
}

function nonsql_error_handler($params) {
    global $app, $log;
    $log->err('NONSQL error: ' . $params['error']);
    $app->render('error.html.twig');
    die; // don't want to keep going if a query broke
}

$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache' //__FILE__ is the absolute path.
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');


\Slim\Route::setDefaultConditions(array(
    'id' => '\d+', // same to '\d+'
    'number' => '\d+'
));

function handleException($exception, $httpCode) {
    global $app, $log;
    $app->response()->status($httpCode);
    //echo "<h1>Code returned: $httpCode</h1>";
    $app->render('error.html.twig', array('httpCode' => $httpCode));
    $log->err(get_class($exception) . $exception->getMessage());
    $log->err($exception->getTraceAsString());
    if (get_class($exception) == "MeekroDBException") {
        $log->err("SQL Query: " . $exception->getQuery());
    }
    $callList = preg_split("/\n/", $exception->getTraceAsString());
    foreach ($callList as $call) {
        $log->err("#" . $call);
    }
}

function createParams($params = array()) {
    if (isset($_SESSION['username'])) {
        $params['username'] = $_SESSION['username'];
    }
    return $params;
}

$app->get('/alt', function() use($app) {
    try {
        $count = DB::queryFirstField("SELECT COUNT(id) FROM article");
        $pagesCount = intval(( $count + 4 ) / 5);
        $pagesCount = ($pagesCount === 0) ? 1 : $pagesCount;

        $app->render('alt_index.html.twig', createParams(array(
            'pagination' => array("total" => $pagesCount, "current" => 1)
        )));
    } catch (MeekroDBException $e) {
        handleException($e, 500);
    }
});

// AJAX API CALL - NOT FOR DIRECT ACCESS FROM WEB BROWSER
$app->get('/alt/page/:number', function ($number) use($app) {
    try {
        $articles = DB::query('SELECT article.id, user.username AS author, article.title,'
                        . ' article.body FROM article,user WHERE article.authorId=user.id ORDER BY '
                        . 'article.id DESC LIMIT %i,5', 5 * ($number - 1));
        for ($i = 0; $i < count($articles); $i++) {
            $clean = strip_tags($articles[$i]['body']);
            $articles[$i]['body'] = substr($clean, 0, 150);
            $articles[$i]['body'] .= strlen($clean) > 150 ? "..." : "";
        }
        $app->render('alt_index_page.html.twig', createParams(array('articles' => $articles)
        ));
    } catch (MeekroDBException $e) {
        handleException($e, 500);
    }
});

$app->get('/page/:number', function($number) use($app) {
    try {
        $articles = DB::query('SELECT article.id, user.username AS author, article.title,'
                        . ' article.body FROM article,user WHERE article.authorId=user.id ORDER BY '
                        . 'article.id DESC LIMIT %i,5', 5 * ($number - 1));
        for ($i = 0; $i < count($articles); $i++) {
            $clean = strip_tags($articles[$i]['body']);
            $articles[$i]['body'] = substr($articles[$i]['body'], 0, 150);
            $articles[$i]['body'] .= strlen($clean) > 150 ? "..." : "";
        }
        $count = DB::queryFirstField("SELECT COUNT(id) FROM article");
        $pagesCount = intval(( $count + 4 ) / 5);
        $pagesCount = ($pagesCount === 0) ? 1 : $pagesCount;
        $app->render('index.html.twig', createParams(array('articles' => $articles,
            'pagination' => array("total" => $pagesCount, "current" => $number)
        )));
    } catch (MeekroDBException $e) {
        handleException($e, 500);
    }
});

$app->get('/', function() use ($app) {
    try {
        $articles = DB::query('SELECT article.id, user.username AS author, article.title,'
                        . ' article.body FROM article,user WHERE article.authorId=user.id '
                        . 'ORDER BY article.id DESC LIMIT 5');
        for ($i = 0; $i < count($articles); $i++) {
            $clean = strip_tags($articles[$i]['body']);
            $articles[$i]['body'] = substr($clean, 0, 150);
            $articles[$i]['body'] .= strlen($clean) > 150 ? "..." : "";
        }

        $count = DB::queryFirstField("SELECT COUNT(id) FROM article");
        $pagesCount = intval(( $count + 4 ) / 5);
        $pagesCount = ($pagesCount === 0) ? 1 : $pagesCount;
        $app->render('index.html.twig', createParams(array('articles' => $articles,
            'pagination' => array("total" => $pagesCount, "current" => 1)
        )));
    } catch (MeekroDBException $e) {
        handleException($e, 500);
    }
});

$app->get('/logout', function () use ($app, $log) {
    $username = isset($_SESSION['username']) ? $_SESSION['username'] : "not-logged-in";
    $log->debug("User" . $username . "has logged out");
    session_destroy();
    $app->render('logout.html.twig');    // Do not pass createParams()!!
});

$app->get('/login', function() use($app) {
    $app->render('login.html.twig', createParams());
});

$app->post('/login', function() use($app, $log) {
    try {
        $username = $app->request->params('username');
        $password = $app->request->params('password');
        $errorList = array();

        if (strlen($username) < 2) {
            array_push($errorList, "incorrect username.");
        } elseif (!$user = DB::queryFirstRow("SELECT * FROM user WHERE username=%s", $username)) {
            array_push($errorList, "User doesn't exist, please register");
        } elseif ($user['password'] != $password) {
            array_push($errorList, "Wrong password.");
        }

        if ($errorList) {
            $app->render('login.html.twig', createParams(
                            array(
                                'errorlist' => $errorList,
                                'value' => array('username' => $username)
            )));
        } else {
            $_SESSION['username'] = $user['username'];
            $_SESSION['userId'] = $user['id'];
            $log->debug('User' . $username . 'logged in successfully.');
            $app->render('login_success.html.twig', createParams());
        }
    } catch (MeekroDBException $e) {
        handleException($e, 500);
    }
});

$app->get('/api/username/available/:username', function($username) {
    try {
        //echo "<b><u>I don't know</u></b>" . gettimeofday(TRUE);
        $result = DB::queryFirstRow("SELECT * FROM user WHERE username=%s", $username);
        echo json_encode($result != null);
    } catch (MeekroDBException $e) {
        handleException($e, 500);
    }
});

$app->get('/register', function() use ($app) {
    $app->render('register.html.twig', createParams());
});

$app->post('/register', function() use ($app, $log) {
    try {
        $username = $app->request->params('username');
        $email = $app->request->params('email');
        $pass1 = $app->request->params('pass1');
        $pass2 = $app->request->params('pass2');
        $errorList = array();
        $valueList = array("username" => $username, "email" => $email);

        if (DB::queryFirstRow("SELECT * FROM user WHERE username=%s", $username)) {
            array_push($errorList, "Username already in use, Either login or pick a different username.");
            $valueList['username'] = '';
        }

        if (!preg_match('/^[a-zA-Z][a-z0-9A-Z_]{1,29}$/', $username)) {
            array_push($errorList, "Username must be at least 2 charcter long and "
                    . "consist of letters and numbers. First character must be a letter");
            $valueList['username'] = '';
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            array_push($errorList, "You must provide a valid email");
            $valueList['email'] = '';
        }
        if (strlen($pass1) == 0 || strlen($pass2) == 0) {
            array_push($errorList, "Password cannot be empty.");
        }
        if ($pass1 != $pass2) {
            array_push($errorList, "Passwords must be identical.");
        }
        if ($errorList) {
            $app->render('register.html.twig', createParams(array('errorList' => $errorList, 'value' => $valueList)));
        } else {
            $log->debug('User ' . $username . 'registered successfully.');
            $app->render('register_success.html.twig', createParams());
            DB::insert('user', array(
                'username' => $username,
                'email' => $email,
                'password' => $pass1
            ));
        }
    } catch (MeekroDBException $e) {
        handleException($e, 500);
    }
});

$app->get('/article/:id', function($id) use($app) {
    if (!isset($_SESSION['userId'])) {
        $app->redirect('/forbidden');
    }
    // if the article does not exist show 404
    // if the article was found - render /display it
    try {
        $article = DB::queryOneRow(' SELECT * FROM article WHERE id=%i', $id);
        if ($article) {
            $app->render('article.html.twig', array('article' => $article));
        } else {
            $app->response()->status(404);
            echo "<h1>404, resource not found</h1>";
        }
    } catch (MeekroDBException $e) {
        handleException($e, 500);
    }
});

$app->get('/forbidden', function() use ($app) {
    $app->render('forbidden.html.twig', createParams());
});


$app->get('/article/create', function() use ($app) {
    if (!isset($_SESSION['userId'])) {
        $app->redirect('/forbidden');
    }
    $app->render('article_create.html.twig', createParams());
});

$app->post('/article/create', function() use ($app) {

    if (!isset($_SESSION['userId'])) {
        $app->redirect('/forbidden');
    }

    try {
        $title = $app->request->params('title');
        $body = $app->request->params('body');
        $errorList = array();
        $valueList = array("title" => $title, "body" => $body);

        if (strlen($title) < 4) {
            array_push($errorList, "Title must be at least 4 characters long.");
            $valueList['title'] = '';
        }
        if (strlen($body) < 10) {
            array_push($errorList, "Body must be at least 10 characters long.");
            $valueList['body'] = '';
        }
        if (isset($_FILES["imageFile"])) {
            if ($_FILES["imageFile"]["error"] != UPLOAD_ERR_OK) {
                array_push($errorList, "Error uploading file");
            } else {
                if (strstr($_FILES["imageFile"]["name"], "\/")){
                    array_push($errorList, "Uploading file.");
                } else {
                 $check = getimagesize($_FILES["imageFile"]["tmp_name"]);
                 if(!$check) {
                 array_push($errorList, "Uploaded file doesn't seem to be an image.");
                }
                }
            }
        }
        if ($errorList) {
            $app->render('article_create.html.twig', createParams(array('errorList' => $errorList, 'value' => $valueList)));
        } else {
            // handle the file upload, if there is one
            if (isset($_FILES["imageFile"])) {
                $ext = pathinfo($_FILES["imageFile"]["name"])['extension'];
                $destPath = "media/" . md5($_FILES["imageFile"]["name"]+time()) . "." . "$ext";
                //$destPath = "media/" . $_FILES["imageFile"]["name"];
                if (move_uploaded_file($_FILES["imageFile"]["tmp_name"], $destPath)) {
                                      
                }
            }
            //$log->debug('User ' . "_SESSION['username']" . 'posted new article, ' . "$title" . '.');
            DB::insert('article', array(
                'authorId' => $_SESSION['userId'],
                'title' => $title,
                'body' => $body,
                'imagePath' => $destPath
            ));
            $article_id = DB::insertId();
            $app->render('article_create_success.html.twig', createParams(array('article_id' => $article_id)));
        }
    } catch (MeekroDBException $e) {
        handleException($e, 500);
    }
});



$app->get('/upload', function() use($app, $log) {
    $app->render('upload.html.twig');
});

$app->post('/upload', function() use($app, $log) {
    //$target_dir = "media/";
    //$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
// Check if image file is a actual image or fake image
    $destPath = "media/" . $_FILES["imageFile"]["name"];
    if (isset($_POST["submit"])) {
        //$checkerror = getimagesize($_FILES["imageFile"]["error"]);
        if ($_FILES["imageFile"]["error"] != UPLOAD_ERR_OK) {
            echo "File upload error.";
            $uploadOk = 0;
        }
    }

    if (move_uploaded_file($_FILES["imageFile"]["tmp_name"], $destPath)) {
        echo "The file " . basename($_FILES["imageFile"]["name"]) . "has been uploade.";
    } else {
        echo "Sorry, there was an problem uploading the file.";
    }
});

$app->get('/secret', function() use($app, $log) {
    $app->render('upload.html.twig');
});

$app->post('/secret', function() use($app, $log) {
    if (isset($_POST["submit"])) {
        if ($_FILES["imageFile"]["error"] != UPLOAD_ERR_OK) {
            echo "File upload error.";
            $uploadOk = 0;
        }
    }
    DB::insert('mediaFile', array(
       'filename' =>  $_FILES["imageFile"]["name"],
        'data' => file_get_contents($_FILES["imageFile"]["tmp_name"])
    ));
    $id = DB::insertId();
    echo 'Secret file uploaded. <a href="/secret/'.$id.'">view file</a>';
});

$app->get('/secret/:id', function($id) use($app) {
    $row = DB::queryOneRow("SELECT * FROM mediaFile WHERE id=%i", $id);
    $info = pathinfo($row['fileName']);
    $ext = strtolower($info['extension']);
    $mimeType = "binary/octet-stream";
    switch($ext){
        case 'jpg':
        case 'jpeg':
            $mimeType = "image/jpeg";
            break;
        case 'gif':
            $mimeType = "image/gif";
            break;
        case 'png':
            $mimeType = "image/png";
            break;
    }
    $app->contentType($mimeType);
    // force download if so requested, e.g. /secret/7?download=true
    if($app->request()->params('download')=='true'){
        $app->response()->header('Content-disposition', 'attachment; filename='.$row['fileName']);
    }
    // send the data - raw contents of the file.
    echo $row['data'];
});

$app->get('/nice/:id', function($id) use ($app){
    $app->render('nice.html.twig', array('id'=>$id));
});

require_once 'lib/shoutbox.php';

$app->run();
