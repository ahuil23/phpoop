<?php

$app->get('/shoutbox', function() use($log, $app) {
    $app->render('shoutbox.html.twig');
});


$app->group('/api', function() use($log, $app) {

    $app->get('/shouts', function() use( $log, $app) {
        try {
            $result = DB::query('SELECT * FROM shout ORDER BY id');
            echo json_encode($result, JSON_PRETTY_PRINT);
        } catch (MeekroDBException $e) {
            handleException($e, 500);
        }
    });

    $app->get('/shouts/:id', function($id) use( $app) {
        try {
            $result = DB::queryOneRow('SELECT * FROM shout WHERE id=%i ORDER BY id', $id);
            if (is_null($result)) {
                $app->response()->status(404);
                echo "<h1>404, resource not found</h1>";
            } else {
                echo json_encode($result, JSON_PRETTY_PRINT);
            }
        } catch (MeekroDBException $e) {
            handleException($e, 500);
        }
    });

    $app->post('/shouts', function() use ( $app) {
        try {
            $json = $app->request()->getBody();
            // 'true' tells Meekrodb to translate the data as array, instead of object
            $data = json_decode($json, true);
            // FIXME: verfiy that the data is valid
            // 1. all columns and only the needed columns are there
            // 2. each column has valid content, e.g. a date is formated as a date SQL understands, interger is an integer
            DB::insert('shout', $data);
        } catch (MeekroDBException $e) {
            handleException($e, 500);
        }
    });

    $app->put('/shouts/:id', function($id) use( $app) {
        try {
            $json = $app->request()->getBody();
            $data = json_decode($json, true);
            $data['id'] = $id;
            // 'true' tells Meekrodb to translate the data as array, instead of object
            DB::update('shout', $data, 'id=%i', $id);
            if (DB::affectedRows() == 0) {
                $app->response()->status(404);
                echo "<h1>404, resource not found</h1>";
            }
        } catch (MeekroDBException $e) {
            handleException($e, 500);
        }
    });

    $app->delete('/shouts/:id', function($id) use( $app) {
        try {
            DB::delete('shout', 'id=%i', $id);
        } catch (MeekroDBException $e) {
            handleException($e, 500);
        }
    });
});

